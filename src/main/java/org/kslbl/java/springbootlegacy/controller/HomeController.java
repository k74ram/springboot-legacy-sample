package org.kslbl.java.springbootlegacy.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    @GetMapping(value = "/hello", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getIndex(@RequestParam(required = false) String name) {
        if(null == name || name.isEmpty()) {
            return "Hai!";
        }
        return String.format("Hai %s !", name);
    }
    
}